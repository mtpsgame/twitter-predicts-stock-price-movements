import csv

import numpy as np
from keras.layers import Dense, Dropout, LSTM

import network_variables as variables
import neural_network_generator as lstm
import sentiment_classifier as sentiment
import tweets

# CONSTANTS
stock_price_date_filename = "TSLA.csv"
tweets_filename = "TSLA_tweets.csv"
number_of_tests = 10
train_test_split = 0.8
recurrent_window_size = 3
batch_size = 32
epochs = 1000


def run():
    """
    Runs the model training procedure.
    Assumes tweets have already been gathered (done within tweets.py).
    """
    # LSTM training and evaluation variables.
    x_baseline, y_baseline = variables.get_baseline_variables(stock_price_date_filename, recurrent_window_size)

    # Train a sentiment classifier.
    tweet_classifier = sentiment.train_sentiment_classifier()

    # LSTM training and evaluation variables.
    x_experiment, y_experiment = variables.get_experiment_variables(stock_price_date_filename, tweets_filename,
                                                                    tweet_classifier, recurrent_window_size)

    # Test each defined network architecture.
    for network in range(len(get_network_architectures(x_baseline.shape))):

        print("\n\nNETWORK " + str(network + 1))

        # --------
        # BASELINE
        # --------

        print("BASELINE")

        # Run multiple tests per architecture to get a mean accuracy.
        baseline_accuracies = []
        baseline_confusion_matrix = np.zeros((2, 2))
        for test in range(number_of_tests):
            # Generate, train and evaluate the network.
            result, confusion = lstm.train_and_evaluate_model(x_baseline, y_baseline,
                                                              get_network_architectures(x_baseline.shape)[network],
                                                              train_test_split, batch_size, epochs)

            # Record and print model accuracy.
            accuracy_percentage = result[1] * 100
            baseline_accuracies.append(accuracy_percentage)
            print("Test " + str(test + 1) + " accuracy: " + str(accuracy_percentage) + "%")

            # Add to confusion matrix
            baseline_confusion_matrix += confusion

        print("Mean accuracy: " + str(sum(baseline_accuracies) / number_of_tests) + "%")
        print("Overall confusion matrix: \n" + str(baseline_confusion_matrix) + "\n\n")

        # ----------
        # EXPERIMENT
        # ----------

        print("EXPERIMENT")

        # Run multiple tests per architecture to get a mean accuracy.
        experiment_accuracies = []
        experiment_confusion_matrix = np.zeros((2, 2))
        for test in range(number_of_tests):
            # Generate, train and evaluate the network.
            result, confusion = lstm.train_and_evaluate_model(x_experiment, y_experiment,
                                                              get_network_architectures(x_experiment.shape)[network],
                                                              train_test_split, batch_size, epochs)

            # Record and print model accuracy.
            accuracy_percentage = result[1] * 100
            experiment_accuracies.append(accuracy_percentage)
            print("Test " + str(test + 1) + " accuracy: " + str(accuracy_percentage) + "%")

            # Add to confusion matrix
            experiment_confusion_matrix += confusion

        # Overall model accuracies.
        print("Mean accuracy: " + str(sum(experiment_accuracies) / number_of_tests) + "%")
        print("Overall confusion matrix: \n" + str(experiment_confusion_matrix) + "\n\n")

        with open("Results_lag_" + str(recurrent_window_size) + ".csv", "a") as results_file:
            writer = csv.writer(results_file)
            writer.writerow(["NETWORK " + str(network + 1)])
            writer.writerow(["Baseline"] + baseline_accuracies + ["", baseline_confusion_matrix[0][0],
                                                                  baseline_confusion_matrix[0][1],
                                                                  baseline_confusion_matrix[1][0],
                                                                  baseline_confusion_matrix[1][1]])
            writer.writerow(["Experiment"] + experiment_accuracies + ["", experiment_confusion_matrix[0][0],
                                                                      experiment_confusion_matrix[0][1],
                                                                      experiment_confusion_matrix[1][0],
                                                                      experiment_confusion_matrix[1][1]])
            writer.writerow("")


def get_network_architectures(input_shape):
    """
    Returns the architecture of the neural network.
    :param input_shape: The shape of the input to the network, X.
    :return: The Keras defined architecture (a list of layers).
    """
    networks_to_test = [
        [
            LSTM(20, input_shape=(input_shape[1], input_shape[2]), return_sequences=False),
            Dropout(0.2),
            Dense(1, activation='sigmoid')
        ],
        [
            LSTM(30, input_shape=(input_shape[1], input_shape[2]), return_sequences=False),
            Dropout(0.2),
            Dense(1, activation='sigmoid')
        ],
        [
            LSTM(50, input_shape=(input_shape[1], input_shape[2]), return_sequences=False),
            Dropout(0.2),
            Dense(1, activation='sigmoid')
        ],
        [
            LSTM(20, input_shape=(input_shape[1], input_shape[2]), return_sequences=True),
            Dropout(0.2),
            LSTM(20, input_shape=(input_shape[1], input_shape[2]), return_sequences=False),
            Dropout(0.2),
            Dense(1, activation='sigmoid')
        ],
        [
            LSTM(20, input_shape=(input_shape[1], input_shape[2]), return_sequences=True),
            Dropout(0.2),
            LSTM(30, input_shape=(input_shape[1], input_shape[2]), return_sequences=False),
            Dropout(0.2),
            Dense(1, activation='sigmoid')
        ],
        [
            LSTM(20, input_shape=(input_shape[1], input_shape[2]), return_sequences=True),
            Dropout(0.2),
            LSTM(50, input_shape=(input_shape[1], input_shape[2]), return_sequences=False),
            Dropout(0.2),
            Dense(1, activation='sigmoid')
        ],
        [
            LSTM(30, input_shape=(input_shape[1], input_shape[2]), return_sequences=True),
            Dropout(0.2),
            LSTM(20, input_shape=(input_shape[1], input_shape[2]), return_sequences=False),
            Dropout(0.2),
            Dense(1, activation='sigmoid')
        ],
        [
            LSTM(30, input_shape=(input_shape[1], input_shape[2]), return_sequences=True),
            Dropout(0.2),
            LSTM(30, input_shape=(input_shape[1], input_shape[2]), return_sequences=False),
            Dropout(0.2),
            Dense(1, activation='sigmoid')
        ],
        [
            LSTM(30, input_shape=(input_shape[1], input_shape[2]), return_sequences=True),
            Dropout(0.2),
            LSTM(50, input_shape=(input_shape[1], input_shape[2]), return_sequences=False),
            Dropout(0.2),
            Dense(1, activation='sigmoid')
        ],
        [
            LSTM(50, input_shape=(input_shape[1], input_shape[2]), return_sequences=True),
            Dropout(0.2),
            LSTM(20, input_shape=(input_shape[1], input_shape[2]), return_sequences=False),
            Dropout(0.2),
            Dense(1, activation='sigmoid')
        ],
        [
            LSTM(50, input_shape=(input_shape[1], input_shape[2]), return_sequences=True),
            Dropout(0.2),
            LSTM(30, input_shape=(input_shape[1], input_shape[2]), return_sequences=False),
            Dropout(0.2),
            Dense(1, activation='sigmoid')
        ],
        [
            LSTM(50, input_shape=(input_shape[1], input_shape[2]), return_sequences=True),
            Dropout(0.2),
            LSTM(50, input_shape=(input_shape[1], input_shape[2]), return_sequences=False),
            Dropout(0.2),
            Dense(1, activation='sigmoid')
        ]
    ]
    return networks_to_test


def gather_tweets():
    """
    Runs the procedure to gather the tweets using the Twitter API and save them to a CSV file.
    """
    tweets.gather_tweets_and_save_to_file("\$Tsla I think -http -www",
                                          "201801010000",
                                          "201811302359",
                                          tweets_filename)


if __name__ == "__main__":
    run()
