from keras import backend
from keras.models import Sequential
from sklearn.metrics import confusion_matrix


def train_and_evaluate_model(X, y, layers, train_test_split, batch_size, epochs):
    """
    Trains a recurrent neural network on the stock data prices and returns the accuracy of it.
    :param X: The input variables to the network.
    :param y: The corresponding output of the network.
    :param layers: List of the neural network layers.
    :param train_test_split: Percentage size of training set (as a decimal).
    :param batch_size: The size of a training batch.
    :param epochs: The number of full training cycles (over all the batches).
    :return: The accuracy of the network, the confusion matrix.
    """
    train_size = int(len(X) * train_test_split)
    X_train = X[:train_size]
    y_train = y[:train_size]
    X_test = X[train_size:]
    y_test = y[train_size:]

    model = Sequential()

    for layer in layers:
        model.add(layer)

    model.compile(loss='binary_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])

    model.fit(X_train, y_train, batch_size=batch_size, epochs=epochs, verbose=0)

    evaluation = model.evaluate(X_test, y_test, batch_size=batch_size, verbose=0)

    y_predictions = model.predict_classes(X_test, batch_size=batch_size)
    c_matrix = confusion_matrix(y_test, y_predictions)

    backend.clear_session()

    return evaluation, c_matrix
