load("Resultslag3.csv")
load("Resultslag4.csv")
load("Resultslag5.csv")

number_of_tests_per_network = 10;
number_of_networks_per_file = 24;
number_of_files = 3;
all_results = [Resultslag3, Resultslag4, Resultslag5];
legend_text = {'Baseline lag=3', 'Experiment lag=3', 'Baseline lag=4', 'Experiment lag=4', 'Baseline lag=5', 'Experiment lag=5'};

for file = 1:number_of_files
    
    results_data = all_results(:, (file - 1) * number_of_tests_per_network + 1:file * number_of_tests_per_network);
    mean_chart(1:length(results_data) / 2, 2) = 0;
    std_chart(1:length(results_data) / 2, 2) = 0;
    
    for i = 1:length(results_data) / 2
        baseline_mean = mean(results_data(2 * i - 1, :));
        experiment_mean = mean(results_data(2 * i, :));
        
        baseline_std = std(results_data(2 * i - 1, :));
        experiment_std = std(results_data(2 * i, :));

        mean_chart(i, :) = [baseline_mean, experiment_mean];
        std_chart(i, :) = [baseline_std, experiment_std];
    end
    figure(1);
    hold on;
    plot(mean_chart, 'LineWidth', 2.5);
    figure(2);
    hold on;
    plot(std_chart, 'LineWidth', 2.5);
end

figure(1);
hold off;
xlim([1, length(results_data) / 2]);
set(gca, 'XTick', 1:length(results_data) / 2);
legend(legend_text);
xlabel('Architecture');
ylabel('Mean percentage accuracy');

figure(2);
hold off;
xlim([1, length(results_data) / 2]);
set(gca, 'XTick', 1:length(results_data) / 2);
legend(legend_text);
xlabel('Architecture');
ylabel('Standard deviation');