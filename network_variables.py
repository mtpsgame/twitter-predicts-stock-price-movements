from datetime import datetime, timedelta

import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler

import sentiment_classifier
import tweets


def get_price_data(closing_prices, dates, recurrent_window_size):
    """
    Pre-process the closing prices and puts in a time series format ready for LSTM input.
    The difference between consecutive closing prices is calculated, and a time lag produced.
    Returns a 3D array, (num_datapoints, recurrent_window_size, 1).
    :param closing_prices: The closing prices loaded from CSV.
    :param dates: The dates of the corresponding closing prices.
    :param recurrent_window_size: The number of time lag steps.
    :return: 3D array X, representing time series of differences between closing prices.
    """
    interpolated_closing_price_dates = [datetime.strptime(dates[0], "%Y-%m-%d")]
    interpolated_closing_prices = [closing_prices[0]]

    for i in range(1, len(closing_prices)):
        previous_date = datetime.strptime(dates[i - 1], "%Y-%m-%d")
        current_date = datetime.strptime(dates[i], "%Y-%m-%d")
        if (previous_date + timedelta(days=1)).weekday() == current_date.weekday():
            interpolated_closing_prices.append(closing_prices[i])
            interpolated_closing_price_dates.append(current_date)
        else:
            while True:
                x = interpolated_closing_prices[-1]
                y = closing_prices[i]
                interpolated_closing_prices.append((x + y) / 2)
                interpolated_closing_price_dates.append(interpolated_closing_price_dates[-1] + timedelta(days=1))
                if (interpolated_closing_price_dates[-1] + timedelta(days=1)).weekday() == current_date.weekday():
                    interpolated_closing_prices.append(closing_prices[i])
                    interpolated_closing_price_dates.append(current_date)
                    break

    closing_prices = pd.DataFrame(interpolated_closing_prices)
    closing_prices = closing_prices.diff()

    cols = []
    for i in range(recurrent_window_size):
        cols.insert(0, closing_prices.shift(i))

    x = pd.concat(cols, axis=1)

    x.dropna(inplace=True)
    x.reset_index(inplace=True, drop=True)

    scale = StandardScaler(with_mean=False)
    x = scale.fit_transform(x)
    x = np.array(x)
    x.resize((x.shape[0], recurrent_window_size, 1))
    return x


def get_target(preprocessed_closing_prices, recurrent_window_size):
    """
    Returns the target vector containing the class for each data point in the preprocessed closing prices.
    :param preprocessed_closing_prices: The preprocessed feature array, from get_closing_prices.
    :param recurrent_window_size: The number of time lag steps.
    :return: The target vector (y).
    """
    y = []
    for i in range(1, len(preprocessed_closing_prices)):
        if preprocessed_closing_prices[i, recurrent_window_size - 1, 0] >= 0:
            y.append(1)
        else:
            y.append(0)

    return np.array(y)


def get_baseline_variables(data_filename, recurrent_window_size):
    """
    Returns the X and y variables required for training and testing, for the baseline.
    :param data_filename: The CSV containing the historical stock price data. Uses the column titled 'Close'.
    :param recurrent_window_size: The number of time lag steps.
    :return: The X and y variables.
    """
    raw_data = pd.read_csv(data_filename)
    closing_prices = raw_data['Close'].astype('float64')
    closing_price_dates = raw_data['Date'].astype('str')

    x = get_price_data(closing_prices, closing_price_dates, recurrent_window_size)
    y = get_target(x, recurrent_window_size)

    x = np.delete(x, len(x) - 1, 0)

    return x, y


def get_sentiment_data(daily_sentiments, recurrent_window_size):
    """
    Puts the daily sentiment statistics into a time series format ready for LSTM input.
    Returns a 3D array, (num_datapoints, recurrent_window_size, 1).
    :param daily_sentiments: The daily sentiment analysis statistics, a list of tuples (date, positive_percentage).
    :param recurrent_window_size: The number of time lag steps.
    :return: 3D array X, representing time series of the percentage of positive tweets per day.
    """
    daily_sentiments.sort(key=lambda s: s[0])
    daily_sentiments = pd.DataFrame(np.array([x[1] for x in daily_sentiments]))
    cols = []
    for i in range(recurrent_window_size):
        cols.insert(0, daily_sentiments.shift(i))

    x = pd.concat(cols, axis=1)

    x.dropna(inplace=True)
    x.reset_index(inplace=True, drop=True)

    x = np.array(x)
    x.resize((x.shape[0], recurrent_window_size, 1))
    return x


def get_experiment_variables(stock_price_filename, tweet_filename, classifier, recurrent_window_size):
    """
    Returns the X and y variables required for training and testing, for the experiment network.
    :param stock_price_filename: The CSV containing the historical stock price data. Uses the column titled 'Close'.
    :param tweet_filename: The name of the filename containing the gathered tweets.
    :param classifier: A trained sentiment classifier, that will classify text as 'pos' or 'neg'.
    :param recurrent_window_size: The number of time lag steps.
    :return: The X and y variables.
    """
    x_baseline, y = get_baseline_variables(stock_price_filename, recurrent_window_size)

    daily_tweets = tweets.load_tweets_and_group_by_day(tweet_filename)
    daily_sentiment = sentiment_classifier.create_daily_sentiment_analysis_stats(daily_tweets, classifier)

    x_sentiment = get_sentiment_data(daily_sentiment, recurrent_window_size)

    x_sentiment = np.delete(x_sentiment, len(x_sentiment) - 1, 0)

    y = y[len(x_baseline) - len(x_sentiment):]

    x_baseline = np.delete(x_baseline, range(0, len(x_baseline) - len(x_sentiment)), 0)

    x = np.concatenate([x_baseline, x_sentiment], axis=2)

    return x, y
