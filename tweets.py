import csv
import os
import re
from datetime import datetime, date

from TwitterAPI import TwitterAPI, TwitterPager


def load_tweets_and_group_by_day(filename, date_format="%a %b %d %H:%M:%S %z %Y", tweet_date_col=0, tweet_text_col=2,
                                 cols_after_tweet_text=1):
    """
    Returns a list of tweets, grouped by the day they were posted.
    :param filename: The name of the CSV file in which the tweets are stored.
    :param date_format: String format of the date / time field.
    :param tweet_date_col: The index (zero based) of the date column.
    :param tweet_text_col: The index (zero based) of the tweet text column.
    :param cols_after_tweet_text: Number of columns after the tweet column. Used to calculate where tweet text ends when
    the tweet contains commas itself.
    :return: A list of tuples, (date, [list_of_tweet_texts]).
    """
    with open(filename, 'r', encoding="utf8") as f:
        reader = csv.reader(f)
        csv_file = list(reader)

    tweet_list = [
        (datetime.strptime(x[tweet_date_col], date_format), ",".join(x[tweet_text_col:len(x) - cols_after_tweet_text]))
        for x in csv_file]

    tweet_list.sort(key=lambda t: t[0])

    daily_tweets = {}

    for tweet in tweet_list:
        tweet_date = date(tweet[0].year, tweet[0].month, tweet[0].day)

        if tweet_date not in daily_tweets:
            daily_tweets[tweet_date] = [tweet[1]]
        else:
            daily_tweets[tweet_date].append(tweet[1])

    return daily_tweets.items()


def gather_tweets_and_save_to_file(query, fromDateUTC, toDateUTC, filename):
    """
    Searches all tweets posted over the last 7 days and returns ones that match the query.
    Assumes the appropriate environment variables have been set to authenticate the connection to the Twitter API.
    :param query: The query that is used to filter the returned tweets.
    :param fromDateUTC: The earliest tweet to gather. A string in the format YYYYMMDDHHmm.
    :param toDateUTC: The latest tweet to gather. A string in the format YYYYMMDDHHmm.
    :param filename: The name of the file to save the tweets to.
    :return: A list of tweets that matched the query.
    """
    api = TwitterAPI(os.environ['CONSUMER_KEY'], os.environ['CONSUMER_SECRET'], os.environ['ACCESS_TOKEN_KEY'],
                     os.environ['ACCESS_TOKEN_SECRET'])

    pager = TwitterPager(api, 'tweets/search/fullarchive/:dev',
                         {'query': query, 'fromDate': fromDateUTC, 'toDate': toDateUTC})

    file = open(filename, 'a', encoding='utf-8')

    try:
        for tweet in pager.get_iterator(wait=5):

            if not 'retweeted_status' in tweet:

                if not 'extended_tweet' in tweet:
                    tweet_text = tweet['text']
                else:
                    tweet_text = tweet['extended_tweet']['full_text']

                tweet_elements = [
                    tweet['created_at'],
                    tweet['id_str'],
                    clean_tweet(tweet_text),
                    tweet['user']['id_str']
                ]

                line = ','.join(tweet_elements) + "\n"
                file.write(line)
    finally:
        file.close()


def clean_tweet(tweet):
    """
    Removes Twitter specific content such as hashtags and usernames, as well as non-meaningful information from the
    tweet text.
    :param tweet: Tweet text to be cleaned.
    :return: The cleaned tweet text.
    """
    tweet = re.sub("@[A-Za-z0-9_]+", "", tweet)  # User names
    tweet = re.sub("\n|\t", " ", tweet)  # New lines and tabs
    tweet = re.sub("((http(s)?://)|www\.)[a-zA-Z0-9./]+", "", tweet)  # URLs
    tweet = re.sub("#", "", tweet)  # Hash tags
    return tweet
